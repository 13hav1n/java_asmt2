package common;

import java.util.HashMap;
import java.util.Map.Entry;


public class Validation {
    public static HashMap<String, String> getEmptyFields(HashMap<String, String> fieldValues){
        HashMap<String,String> emptyFields = new HashMap<>();
       for(Entry<String, String> entry : fieldValues.entrySet()){
           if(entry.getValue().isEmpty()){
               emptyFields.put(entry.getKey(), entry.getValue());
           }
       }
       return emptyFields;
    }
    public static HashMap<String, String> getPopulatedFields(HashMap<String, String> fieldValues){
        HashMap<String,String> filled = new HashMap<>();
       for(Entry<String, String> entry : fieldValues.entrySet()){
           if(!entry.getValue().isEmpty()){
               filled.put(entry.getKey(), entry.getValue());
           }
       }
       return filled;
    }
}

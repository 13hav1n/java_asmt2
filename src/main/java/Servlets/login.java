package Servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "login", urlPatterns = {"/login"})
public class login extends HttpServlet {

    HashMap<String, String> previousValues = new HashMap<>();

    protected String persistValue(String fieldName) {
        if (!previousValues.isEmpty()) {
            if (previousValues.get(fieldName) != null) {
                return previousValues.get(fieldName);
            }
            return "";
        }
        return "";
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");        
        ServletContext appContext = this.getServletContext();

        appContext.setAttribute("formOperation", "registration");

        if (appContext.getAttribute("filledFields") != null) {
            previousValues = (HashMap) appContext.getAttribute("filledFields");
        }
        appContext.setAttribute("formOperation", "login");
        try (PrintWriter out = response.getWriter()) {
            out.println("<html>\n"
                    + "<head>\n"
                    + "<title>Ecom Any Time</title>          \n"
                    + "<link rel='stylesheet' href='css/logins.css'></link>          \n"
                    + "<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css'></link>       \n"
                    + "</head>\n"
                    + "<body>\n");
            if (appContext.getAttribute("message") != null) {
                out.println("<div class='container'>");
                if (String.valueOf(appContext.getAttribute("message")).toLowerCase().contains("success")) {
                    out.println("<div class=\"mt-5 alert alert-success alert-dismissible  fade show\" role=\"alert\">\n"
                            + "You have been successfully registered!\n"
                            + "<button class=\"btn-close\"  data-bs-dismiss=\"alert\" aria-label=\"Close\"></button>"
                            + "</div>");
                } else if (String.valueOf(appContext.getAttribute("message")).toLowerCase().contains("invalid") || String.valueOf(appContext.getAttribute("message")).toLowerCase().contains("unable")) {
                    out.println("<div class=\"mt-5 alert alert-danger alert-dismissible  fade show\" role=\"alert\">\n"
                            + "Invalid login credentials!\n"
                            + "<button class=\"btn-close\"  data-bs-dismiss=\"alert\" aria-label=\"Close\"></button>"
                            + "</div>");
                }
                out.println("</div>");
                appContext.removeAttribute("message");
                appContext.removeAttribute("filledFields");
            }
            out.println("    <div class=\"shadow p-4 rounded-3 position-absolute top-50 start-50 translate-middle\" style=\"width:30rem;background-color: white;\">\n"
                    + "        <div class=\"fs-2 mb-3\">Login</div>\n");
            if (appContext.getAttribute("validation") != null) {
                if (appContext.getAttribute("validation").equals(false)) {
                    out.println("<div class=\"alert alert-danger\" role=\"alert\">\n"
                            + "  Some fileds are empty!\n"
                            + "</div>");
                    appContext.setAttribute("validation", true);
                }
            }
            out.println("        <form class=\"m-0\" method=\"post\" action=\"validate\">\n"
                    + "            <div class=\"mb-3\">\n"
                    + "                <label for=\"email\" class=\"form-label\">Email address</label>\n"
                    + "                <input type=\"email\" class=\"form-control\" id=\"email\" name=\"email\" value='" + persistValue("email") + "'>\n"
                    + "            </div>\n"
                    + "            <div class=\"mb-3\">\n"
                    + "                <label for=\"password\" class=\"form-label\">Password</label>\n"
                    + "                <input type=\"password\" class=\"form-control\" id=\"password\" name=\"password\" value='" + persistValue("password") + "'>\n"
                    + "            </div>\n"
                    + "            <div class=\"d-flex justify-content-between\">\n"
                    + "                <div class=\"mb-3 form-check\">\n"
                    + "                    <input type=\"checkbox\" class=\"form-check-input\" name=\"remme\" id=\"check\">\n"
                    + "                    <label class=\"form-check-label\" for=\"remme\">Remember me</label>\n"
                    + "                </div>\n"
                    + "                <div>\n"
                    + "                    <a href=\"forgetPassword\" class=\"link px-2\">Forget password?</a>\n"
                    + "\n"
                    + "                </div>\n"
                    + "            </div>\n"
                    + "            <div class=\"d-flex\">\n"
                    + "                <button type=\"submit\" class=\"btn btn-primary px-4\" id=\"loginBtn\">Login</button>\n"
                    + "                <a href=\"register\" class=\" btn btn-outline-primary ms-2 px-4 align-self-center\">Sign up</a>\n"
                    + "            </div>\n"
                    + "        </form>\n"
                    + "    </div>\n"
                    + "<script src='https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js'></script>\n"
                    + "</body>\n"
                    + "</html>");
            previousValues.clear();
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
}

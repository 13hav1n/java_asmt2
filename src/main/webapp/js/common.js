const username = document.getElementById("username");
const email = document.getElementById("email");
const phone = document.getElementById("phone");
const password = document.getElementById("password");
const pass_question = document.getElementById("password_question");
const pass_answer = document.getElementById("password_answer");
const address = document.getElementById("address");
const city = document.getElementById("city");
const state = document.getElementById("state");
const country = document.getElementById("country");
const pin = document.getElementById("pin");
const captcha = document.querySelector(".g-recaptcha");
const registerBtn = document.querySelector("#registerBtn");

country.addEventListener("change", () => {
    loadData("state");
});
state.addEventListener("change", () => {
    loadData("city");
});

async function loadData(operation) {
    try {
        const response = await fetch(`gsc?op=${operation}&id=${operation === "state" ? country.value : state.value}`);
        const result = await response.json();
        const options = result
                .map((item) => `<option value='${item.id}'>${item.name}</option>`)
                .join(" ");
        if (operation === "state") {
            state.innerHTML = options;
        }
        if (operation === "city") {
            city.innerHTML = options;
        }
    } catch (ex) {
        console.log("Exception in fetching cities list : ", ex);
    }
}

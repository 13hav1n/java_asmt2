-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jan 30, 2023 at 05:09 PM
-- Server version: 8.0.31
-- PHP Version: 8.0.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `a2store`
--

-- --------------------------------------------------------

--
-- Table structure for table `category_master`
--

DROP TABLE IF EXISTS `category_master`;
CREATE TABLE IF NOT EXISTS `category_master` (
  `category_id` int NOT NULL AUTO_INCREMENT,
  `category_name` varchar(50) COLLATE utf8_general_ci DEFAULT NULL,
  `parent_category_id` int DEFAULT NULL,
  PRIMARY KEY (`category_id`),
  KEY `parent_category_id` (`parent_category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `city_master`
--

DROP TABLE IF EXISTS `city_master`;
CREATE TABLE IF NOT EXISTS `city_master` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `state_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `state_id` (`state_id`)
) ENGINE=MyISAM AUTO_INCREMENT=65 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dumping data for table `city_master`
--

INSERT INTO `city_master` (`id`, `name`, `state_id`) VALUES
(1, 'Mumbai', 1),
(2, 'Nasik', 1),
(3, 'Aurangabad', 1),
(4, 'Nagpur', 1),
(5, 'Amritsar', 2),
(6, 'Jalandhar', 2),
(7, 'Patiala', 2),
(8, 'Ludhiana', 2),
(9, 'Surat', 3),
(10, 'Vadodra', 3),
(11, 'Ahamdavad', 3),
(12, 'Junagadh', 3),
(13, 'Thiruvanantpuram', 4),
(14, 'Trissur', 4),
(15, 'Kochi', 4),
(16, 'Kollam', 4),
(17, 'Hobart', 9),
(18, 'Launceston', 9),
(19, 'Burnie', 9),
(20, 'Queenstown', 9),
(21, 'Melbourne', 10),
(22, 'Ballarat', 10),
(23, 'Geelong', 10),
(24, 'Ararat', 10),
(25, 'Sydney', 11),
(26, 'Maitland', 11),
(27, 'Broken Hill', 11),
(28, 'Orange', 11),
(29, 'Mackay', 12),
(30, 'Brisbane', 12),
(31, 'Townsville', 12),
(32, 'Rockhampton', 12),
(33, 'Los Angeles', 13),
(34, 'San Diego', 13),
(35, 'Torrance', 13),
(36, 'San Francisco', 13),
(37, 'Houston', 14),
(38, 'San Antonio', 14),
(39, 'Dallas', 14),
(40, 'EL Paso', 14),
(41, 'Jacksonville', 15),
(42, 'Miami', 15),
(43, 'Tampa', 15),
(44, 'Orlando', 15),
(45, 'Honolulu', 16),
(46, 'Hilo', 16),
(47, 'Volcano', 16),
(48, 'Kapolei', 16),
(49, 'London', 17),
(50, 'Bristol', 17),
(51, 'Nottingham', 17),
(52, 'Birmingham', 17),
(53, 'Jacksonville', 18),
(54, 'Miami', 18),
(55, 'Inverness', 18),
(56, 'Perth', 18),
(57, 'Adamsdown', 19),
(58, 'Caerau', 19),
(59, 'Penylan', 19),
(60, 'Butetown', 19),
(61, 'Thurso', 20),
(62, 'Evanton', 20),
(63, 'Golspie', 20),
(64, 'Castletown', 20);

-- --------------------------------------------------------

--
-- Table structure for table `country_master`
--

DROP TABLE IF EXISTS `country_master`;
CREATE TABLE IF NOT EXISTS `country_master` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dumping data for table `country_master`
--

INSERT INTO `country_master` (`id`, `name`) VALUES
(1, 'India'),
(2, 'Australia'),
(3, 'United states of america (USA)'),
(4, 'United kingdom (UK)');

-- --------------------------------------------------------

--
-- Table structure for table `order_details`
--

DROP TABLE IF EXISTS `order_details`;
CREATE TABLE IF NOT EXISTS `order_details` (
  `order_detail_id` int NOT NULL AUTO_INCREMENT,
  `order_id` int DEFAULT NULL,
  `product_id` int DEFAULT NULL,
  `product_price` int DEFAULT NULL,
  `discount` int DEFAULT NULL,
  PRIMARY KEY (`order_detail_id`),
  KEY `order_id` (`order_id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `order_master`
--

DROP TABLE IF EXISTS `order_master`;
CREATE TABLE IF NOT EXISTS `order_master` (
  `order_id` int NOT NULL AUTO_INCREMENT,
  `order_datetime` varchar(50) COLLATE utf8_general_ci DEFAULT NULL,
  `session_id` varchar(255) COLLATE utf8_general_ci DEFAULT NULL,
  `payment_mode` varchar(50) COLLATE utf8_general_ci DEFAULT NULL,
  `tax` int DEFAULT NULL,
  `total_amount` int DEFAULT NULL,
  `order_status` varchar(50) COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `parent_category`
--

DROP TABLE IF EXISTS `parent_category`;
CREATE TABLE IF NOT EXISTS `parent_category` (
  `parent_category_id` int NOT NULL AUTO_INCREMENT,
  `parent_category_name` varchar(50) COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`parent_category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_master`
--

DROP TABLE IF EXISTS `product_master`;
CREATE TABLE IF NOT EXISTS `product_master` (
  `product_id` int NOT NULL AUTO_INCREMENT,
  `propduct_name` varchar(50) COLLATE utf8_general_ci DEFAULT NULL,
  `price` int DEFAULT NULL,
  `discount` int DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_general_ci DEFAULT NULL,
  `stock` int DEFAULT NULL,
  `category_id` int DEFAULT NULL,
  PRIMARY KEY (`product_id`),
  KEY `category_id` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `state_master`
--

DROP TABLE IF EXISTS `state_master`;
CREATE TABLE IF NOT EXISTS `state_master` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `country_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `country_id` (`country_id`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dumping data for table `state_master`
--

INSERT INTO `state_master` (`id`, `name`, `country_id`) VALUES
(1, 'Maharastra', 1),
(2, 'Punjab', 1),
(3, 'Gujarat', 1),
(4, 'Kerala', 1),
(10, 'Victoria', 2),
(9, 'Tasmania', 2),
(11, 'New south wales', 2),
(12, 'Queensland', 2),
(13, 'California', 3),
(14, 'Texas', 3),
(15, 'Florida', 3),
(16, 'Hawaii', 3),
(17, 'England', 4),
(18, 'Scotland', 4),
(19, 'Kardiff', 4),
(20, 'Highland', 4);

-- --------------------------------------------------------

--
-- Table structure for table `user_master`
--

DROP TABLE IF EXISTS `user_master`;
CREATE TABLE IF NOT EXISTS `user_master` (
  `user_id` int NOT NULL AUTO_INCREMENT,
  `user_name` varchar(50) COLLATE utf8_general_ci DEFAULT NULL,
  `password` varchar(50) COLLATE utf8_general_ci DEFAULT NULL,
  `password_question` varchar(50) COLLATE utf8_general_ci DEFAULT NULL,
  `password_answer` varchar(50) COLLATE utf8_general_ci DEFAULT NULL,
  `email` varchar(150) COLLATE utf8_general_ci DEFAULT NULL,
  `phone` varchar(50) COLLATE utf8_general_ci DEFAULT NULL,
  `addresss` varchar(255) COLLATE utf8_general_ci DEFAULT NULL,
  `city` varchar(50) COLLATE utf8_general_ci DEFAULT NULL,
  `state` varchar(50) COLLATE utf8_general_ci DEFAULT NULL,
  `country` varchar(50) COLLATE utf8_general_ci DEFAULT NULL,
  `pin` int DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `category_master`
--
ALTER TABLE `category_master`
  ADD CONSTRAINT `category_master_ibfk_1` FOREIGN KEY (`parent_category_id`) REFERENCES `parent_category` (`parent_category_id`);

--
-- Constraints for table `order_details`
--
ALTER TABLE `order_details`
  ADD CONSTRAINT `order_details_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `order_master` (`order_id`),
  ADD CONSTRAINT `order_details_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `product_master` (`product_id`);

--
-- Constraints for table `product_master`
--
ALTER TABLE `product_master`
  ADD CONSTRAINT `product_master_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `category_master` (`category_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

package Servlets;

import java.sql.*;
import common.DbOperations;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "register", urlPatterns = {"/register"})
public class register extends HttpServlet {

    protected String getSelected(HashMap<String, String> prevVal, String fieldName, String valueToCompare) {
        if (!prevVal.isEmpty()) {
            if (prevVal.get(fieldName) != null) {
                if (prevVal.get(fieldName).equals(valueToCompare)) {
                    return "selected";
                }
                return "";
            }
            return "";
        }
        return "";
    }

    protected String persistValue(HashMap<String, String> prevVal, String fieldName) {
        if (!prevVal.isEmpty()) {
            if (prevVal.get(fieldName) != null) {
                return prevVal.get(fieldName);
            }
            return "";
        }
        return "";
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        DbOperations dbo = new DbOperations();
        response.setContentType("text/html;charset=UTF-8");
        ServletContext sContext = this.getServletContext();

        sContext.setAttribute("formOperation", "registration");

        HashMap<String, String> previousValues = new HashMap<>();
        if (sContext.getAttribute("filledFields") != null) {
            previousValues = (HashMap) sContext.getAttribute("filledFields");
        }

        try (PrintWriter out = response.getWriter()) {

            out.println("<html>\n"
                    + "<head>\n"
                    + "<title>Ecom Any Time</title>          \n"
                    + "<link rel='stylesheet' href='css/logins.css'></link>          \n"
                    + "<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css'></link>       \n"
                    + "</head>\n"
                    + "<script src=\"https://www.google.com/recaptcha/api.js\" async defer></script>\n"
                    + "<script src='js/common.js' defer></script>"
                    + "<body>\n"
                    + "    <div class=\"shadow p-4 rounded-3 position-absolute top-50 start-50 translate-middle\" style=\"width:30rem; height:90%;overflow:auto;background-color: white;\">\n"
                    + "        <div class=\"fs-2 mb-3\">Registration</div>\n");
            if (sContext.getAttribute("validation") != null) {
                if (sContext.getAttribute("validation").equals(false)) {
                    out.println("<div class=\"alert alert-danger\" role=\"alert\">\n"
                            + "  Some fileds are empty!\n"
                            + "</div>");
                    sContext.setAttribute("validation", true);
                }
            }
            out.println("        <form class=\"m-0\" method=\"post\" action=\"validate\">\n"
                    + "            <div class=\"mb-3\">\n"
                    + "                <label for=\"username\" class=\"form-label\">Create username</label>\n"
                    + "                <input type=\"text\" class=\"form-control\" id=\"username\" name=\"username\" value='" + persistValue(previousValues, "username") + "'/>");

            out.println("            </div>\n"
                    + "            <div class=\"mb-3\">\n"
                    + "                <label for=\"email\" class=\"form-label\">Email address</label>\n"
                    + "                <input type=\"email\" class=\"form-control\" id=\"email\" name=\"email\" value='" + persistValue(previousValues, "email") + "'/>\n"
                    + "            </div>\n"
                    + "            <div class=\"mb-3\">\n"
                    + "                <label for=\"phone\" class=\"form-label\">Phone number</label>\n"
                    + "                <input type=\"number\" class=\"form-control\" id=\"phone\" name=\"phone\" value='" + persistValue(previousValues, "phone") + "'>\n"
                    + "            </div>\n"
                    + "            <div class=\"mb-3\">\n"
                    + "                <label for=\"password\" class=\"form-label\">Enter password</label>\n"
                    + "                <input type=\"password\" class=\"form-control\" id=\"password\" name=\"password\" value='" + persistValue(previousValues, "passsword") + "'/>\n"
                    + "            </div>\n"
                    + "            <div class=\"mb-3\">\n"
                    + "                <select class=\"form-select\" name=\"password_question\">\n"
                    + "                    <option selected value=\"\">Select password question</option>\n"
                    + "                    <option value=\"What is your favourite food?\" " + getSelected(previousValues, "password_question", "What is your favourite food?") + ">What is your favourite food?</option>\n"
                    + "                    <option value=\"What is your favourite color?\" " + getSelected(previousValues, "password_question", "What is your favourite color?") + ">What is your favourite color?</option>\n"
                    + "                    <option value=\"Your childhood teacher name?\" " + getSelected(previousValues, "password_question", "Your childhood teacher name?") + ">Your childhood teacher name?</option>\n"
                    + "                    <option value=\"Your favourite dog breed?\" " + getSelected(previousValues, "password_question", "Your favourite dog breed?") + ">Your favourite dog breed?</option>\n"
                    + "                    <option value=\"Your favourite subject in school?\" " + getSelected(previousValues, "password_question", "Your favourite subject in school?") + ">Your favourite subject in school?</option>\n"
                    + "                    <option value=\"What was your favourite moive last year?\" " + getSelected(previousValues, "password_question", "What was your favourite moive last year?") + ">What was your favourite moive last year?</option>\n"
                    + "                </select>\n"
                    + "            </div>\n"
                    + "            <div class=\"mb-3\">\n"
                    + "                <label for=\"password_answer\" class=\"form-label\">Password answer</label>\n"
                    + "                <input type=\"text\" class=\"form-control\" id=\"password_answer\" name=\"password_answer\" value='" + persistValue(previousValues, "password_answer")+ "'/>\n"
                    + "            </div>\n"
                    + "            <div class=\"mb-3\">\n"
                    + "                <label for=\"address\" class=\"form-label\">Enter address</label>\n"
                    + "                <textarea class=\"form-control\" id=\"address\" rows=\"3\" name=\"address\">" + persistValue(previousValues, "address") + "</textarea>\n"
                    + "            </div>\n"
                    + "            <div class=\"mb-3\">\n"
                    + "                <select class=\"form-select\" name=\"country\" id=\"country\">\n"
                    + "                    <option selected value=\"\">Select country</option>\n");
            try {
                ResultSet countries = dbo.getCountryList();
                while (countries.next()) {
                    out.println("<option value='" + countries.getInt("id") + "'>" + countries.getString("name") + "</option>");
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
            out.println("                </select>\n"
                    + "            </div>\n"
                    + "            <div class=\"mb-3\">\n"
                    + "                <select class=\"form-select\" name=\"state\" id=\"state\">\n"
                    + "                    <option selected value=\"\">Select state</option>\n"
                    + "                </select>\n"
                    + "            </div>\n"
                    + "            <div class=\"mb-3\">\n"
                    + "                <select class=\"form-select\" name=\"city\" id=\"city\">\n"
                    + "                    <option selected value=\"\">Select city</option>\n"
                    + "                </select>\n"
                    + "            </div>\n"
                    + "            <div class=\"mb-3\">\n"
                    + "                <label for=\"pin\" class=\"form-label\">Pin number</label>\n"
                    + "                <input type=\"number\" class=\"form-control\" id=\"pin\" name=\"pin\" value='" + persistValue(previousValues, "pin") + "'>\n"
                    + "            </div>\n"
                    + "            <div class=\"mb-3\">\n"
                    + "               <div class=\"g-recaptcha\" data-sitekey=\"6LeAtjMkAAAAABzfZkTYP_mFBhIjSxJTT_csNHnr\"></div>\n"
                    + "            </div>            \n"
                    + "            <div class=\"d-flex\">\n"
                    + "                <button type=\"submit\" class=\"btn btn-primary px-4\" id=\"registerBtn\">Create</button>\n"
                    + "            </div>\n"
                    + "        </form>\n"
                    + "    </div>\n"
                    + "<script src='https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js'></script>\n"
                    + "\n"
                    + "</body>\n"
                    + "</html>");
            previousValues.clear();
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);

    }
}

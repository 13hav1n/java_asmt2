package Servlets;

import common.DbOperations;
import common.Validation;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map.Entry;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "validate", urlPatterns = {"/validate"})

public class validate extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        DbOperations dbo = new DbOperations();
        ServletContext appContext = this.getServletContext();
        if (appContext.getAttribute("formOperation").equals("registration")) {
            ArrayList<String> params = Collections.list(request.getParameterNames());
            HashMap<String, String> fieldValues = new HashMap<>();
            for (String key : params) {
                fieldValues.put(key, request.getParameter(key).toString());
            }
            HashMap<String, String> emptyFields = Validation.getEmptyFields(fieldValues);
            HashMap<String, String> populatedFields = Validation.getPopulatedFields(fieldValues);

            if (emptyFields.size() != 0) {
                appContext.setAttribute("validation", false);
                appContext.setAttribute("filledFields", populatedFields);
                response.sendRedirect("register");
//                RequestDispatcher reqDesp = request.getRequestDispatcher("register");
//                reqDesp.forward(request, response);
            } else {
                try {
                    dbo.ps = dbo.con.prepareStatement("insert into user_master (user_name, password, password_question, password_answer, email, phone, address, city, state, country, pin) values (?,?,?,?,?,?,?,?,?,?,?)");
                    String city = dbo.getSingleStringItem("city_master", request.getParameter("city"), "id", "name");
                    String state = dbo.getSingleStringItem("state_master", request.getParameter("state"), "id", "name");
                    String country = dbo.getSingleStringItem("country_master", request.getParameter("country"), "id", "name");
                    dbo.ps.setString(1, request.getParameter("username"));
                    dbo.ps.setString(2, request.getParameter("password"));
                    dbo.ps.setString(3, request.getParameter("password_question"));
                    dbo.ps.setString(4, request.getParameter("password_answer"));
                    dbo.ps.setString(5, request.getParameter("email"));
                    dbo.ps.setString(6, request.getParameter("phone"));
                    dbo.ps.setString(7, request.getParameter("address"));
                    dbo.ps.setString(8, city);
                    dbo.ps.setString(9, state);
                    dbo.ps.setString(10, country);
                    dbo.ps.setInt(11, Integer.parseInt(request.getParameter("pin")));
                    int status = dbo.ps.executeUpdate();
                    if (status == 1) {
                        appContext.setAttribute("message", "User has been successfully registered!");
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                response.sendRedirect("login");
                appContext.setAttribute("validation", true);
//                RequestDispatcher reqDesp = request.getRequestDispatcher("home");
//                reqDesp.forward(request, response);
            }
        } else if (appContext.getAttribute("formOperation").equals("login")) {
            ArrayList<String> params = Collections.list(request.getParameterNames());
            HashMap<String, String> fieldValues = new HashMap<>();
            for (String key : params) {
                fieldValues.put(key, request.getParameter(key).toString());
            }
            HashMap<String, String> emptyFields = Validation.getEmptyFields(fieldValues);
            HashMap<String, String> populatedFields = Validation.getPopulatedFields(fieldValues);
            if (emptyFields.size() != 0) {
                appContext.setAttribute("validation", false);
                appContext.setAttribute("filledFields", populatedFields);
                response.sendRedirect("login");
            } else {
                if (request.getParameter("email").equals("admin@store.com") && request.getParameter("password").equals("admin432")) {
                    appContext.setAttribute("validation", true);
                    HttpSession session = request.getSession();
                    session.setAttribute("user", request.getParameter("email"));
                    session.setAttribute("login", true);
                    response.sendRedirect("panel");
                    return;
                }
                ResultSet result = dbo.getResultSetFromQuery("select * from user_master where email = '" + request.getParameter("email") + "'");
                try {
                    if (result != null) {
                        result.next();
                        String userEmail = result.getString("email");
                        String password = result.getString("password");
                        if (userEmail.equals(request.getParameter("email")) && password.equals(request.getParameter("password"))) {
                            appContext.setAttribute("validation", true);
                            HttpSession session = request.getSession();
                            session.setAttribute("user", request.getParameter("email"));
                            session.setAttribute("login", true);
                            response.sendRedirect("home");
                        } else {
                            appContext.setAttribute("validation", false);
                            appContext.setAttribute("message", "Invalid login credentials!");
                            response.sendRedirect("login");
                        }
                    }
                } catch (Exception ex) {
                    System.out.println("Sql exception: " + ex.getMessage());
                    appContext.setAttribute("validation", false);
                    appContext.setAttribute("message", "Invalid login credentials!");
                    response.sendRedirect("login");
                }

            }
        } else {
        }
    }
}

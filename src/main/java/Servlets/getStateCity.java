package Servlets;

import com.google.gson.Gson;
import java.sql.*;
import common.DbOperations;
import common.StateCity;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "getStateCity", urlPatterns = {"/gsc"})
public class getStateCity extends HttpServlet {

    DbOperations dbo = null;

    public getStateCity() {
        this.dbo = new DbOperations();
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String operation = request.getParameter("op");
        String id = request.getParameter("id");
        if (operation.equals("state")) {
            ArrayList<StateCity> states = dbo.getStateOfCountry(id);
            Gson json = new Gson();
            String lstStates = json.toJson(states);
            response.setContentType("text/json");
            response.getWriter().write(lstStates);

        }

        if (operation.equals("city")) {
            ArrayList<StateCity> cities = dbo.getCityOfState(id);
            Gson json = new Gson();
            String lstCities = json.toJson(cities);
            response.setContentType("text/json");
            response.getWriter().write(lstCities);

        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }
}

package Servlets;

import common.DbOperations;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "forgetPassword", urlPatterns = {"/forgetPassword"})
public class forgetPassword extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            DbOperations dbo = new DbOperations();
            ServletContext appContext = this.getServletContext();
            out.println("<!DOCTYPE html>");
            out.println("<html lang=\"en-US\">");
            out.println("<head>\n"
                    + "        <meta charset=\"UTF-8\">\n"
                    + "        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n"
                    + "        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n"
                    + "        <link rel=\"stylesheet\" href=\"css/logins.css\">\n"
                    + "        <link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css'></link>\n"
                    + "        <title>Ecom Any Time</title>\n"
                    + "    </head>");
            out.println("<body>\n"
                    + "    <div class=\"shadow p-4 rounded-3 position-absolute top-50 start-50 translate-middle\" style=\"width:30rem;background-color: white;\">\n"
                    + "        <form class=\"m-0\" method=\"post\">\n"
                    + "                        <div class=\"mb-3\">\n"
                    + "                            <label for=\"email\" class=\"form-label\">Email address</label>\n"
                    + "                            <input type=\"email\" class=\"form-control\" id=\"email\" name=\"email\" value=\"\">\n"
                    + "                        </div>\n"
                    + "                        <div class=\"d-flex justify-content-end\">\n"
                    + "                            <button type=\"button\" class=\"btn btn-primary px-4\" id=\"verify\">Apply</button>\n"
                    + "                        </div>\n"
                    + "                        <div class=\"mt-3 d-none\" id=\"Change-Password-Block\">\n"
                    + "                            <div class=\"mb-3\">\n"
                    + "                                <label for=\"newPass\" class=\"form-label\">Enter new password</label>\n"
                    + "                                <input type=\"password\" class=\"form-control\" id=\"newPass\" name=\"newPass\" value=\"\">\n"
                    + "                            </div>\n"
                    + "                            <div class=\"mb-3\">\n"
                    + "                                <label for=\"confirmPass\" class=\"form-label\">Confirm password</label>\n"
                    + "                                <input type=\"password\" class=\"form-control\" id=\"confirmPass\" name=\"confirmPass\" value=\"\">\n"
                    + "                            </div>\n"
                    + "                            <div class=\"d-flex justify-content-end\">\n"
                    + "                                <button type=\"submit\" class=\"btn btn-primary px-4\" id=\"change-pass\">Change password</button>\n"
                    + "                            </div>\n"
                    + "                        </div>\n"
                    + "                    </form>\n"
                    + "    </div>\n"
                    + "</body>");
            out.println("<script src='https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js'></script>");
            out.println("<script src='js/forgetpass.js'></script>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
}

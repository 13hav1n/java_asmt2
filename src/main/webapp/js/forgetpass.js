const targetEmail = document.getElementById("email");
const verifyBtn = document.getElementById("verify");
const changePassBlock = document.getElementById("Change-Password-Block");
verifyBtn.addEventListener("click", (e) => {
    e.preventDefault();
    if (targetEmail.value.trim()) {
        changePassBlock.classList.remove('d-none');
    } else {
        const errorSpan = document.createElement('span');
        errorSpan.setAttribute("style", "color:red");
        errorSpan.textContent = "Email is required!"
        targetEmail.parentElement.appendChild(errorSpan);
    }
});
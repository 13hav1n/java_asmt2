package common;

import java.sql.*;
import java.util.*;

/**
 *
 * @author Lenovo
 */
public class DbOperations {

    public Connection con = null;
    public PreparedStatement ps;
    public Statement statement;

    public DbOperations() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            this.con = DriverManager.getConnection("jdbc:mysql://localhost:3306/a2store?useSSL=false", "root", "root");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void closeConnection() {
        try {
            if (con != null) {
                con.close();
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
        }
    }

    public ResultSet getResultSetFromQuery(String qry) {
        ResultSet result = null;
        try {
            statement = con.createStatement();
            statement.executeQuery(qry);
            result = statement.getResultSet();
        } catch (SQLException ex) {
            System.out.println(ex);
            ex.printStackTrace();
        } finally {
            return result;
        }
    }

    public ResultSet getResult(String tableName) {
        ResultSet result = null;
        try {
            statement = con.createStatement();
            statement.executeQuery("select * from " + tableName);
            result = statement.getResultSet();
        } catch (SQLException ex) {
            System.out.println(ex);
            ex.printStackTrace();
        } finally {
            return result;
        }
    }

    public ResultSet getCountryList() {
        ResultSet result = null;
        try {
            statement = con.createStatement();
            statement.executeQuery("select * from country_master");
            result = statement.getResultSet();
        } catch (SQLException ex) {
            System.out.println(ex);
            ex.printStackTrace();
        } finally {
            return result;
        }
    }

    public ArrayList<StateCity> getStateOfCountry(String countryId) {
        ArrayList<StateCity> states = new ArrayList<>();
        try {
            statement = con.createStatement();
            statement.executeQuery("select * from state_master where country_id = '" + countryId + "'");
            ResultSet result = statement.getResultSet();
            while (result.next()) {
                states.add(new StateCity(result.getInt("id"), result.getString("name")));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            return states;
        }
    }

    public ArrayList<StateCity> getCityOfState(String stateId) {
        ArrayList<StateCity> cities = new ArrayList<>();
        try {
            statement = con.createStatement();
            statement.executeQuery("select * from city_master where state_id = '" + stateId + "'");
            ResultSet result = statement.getResultSet();
            while (result.next()) {
                cities.add(new StateCity(result.getInt("id"), result.getString("name")));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            return cities;
        }
    }

    public String getSingleStringItem(String tableName, String sourceValue, String comparatorColumn, String columnToFetch) {
        String stringResult = "";
        try {
            statement = con.createStatement();
            statement.executeQuery("select * from " + tableName + " where " + comparatorColumn + " = " + "'" + sourceValue + "'");
            ResultSet rs = statement.getResultSet();
            while (rs.next()) {
                stringResult = rs.getString(columnToFetch);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            return stringResult;
        }
    }
}
